const Koa = require("koa")
const { gql, ApolloServer } = require("apollo-server-koa")
const fs = require("fs")
const router = require("koa-router")()
const json = require("koa-json")
const port = process.env.PORT || 4000

const typeDefs = gql(
  fs.readFileSync("./src/schema.graphql", { encoding: "UTF-8" })
)
const resolvers = require("./src/resolvers")
const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ ctx }) => ({
    method: ctx.request.method,
    headers: ctx.request.headers
  }),
  introspection: true
})

const app = new Koa()
app.use(router.routes())
app.use(json())
server.applyMiddleware({ app })

router.get("/hello", ctx => {
  ctx.body = { message: "hello" }
})

app.listen({ port }, () =>
  console.log(
    `🚀 Server ready at http://localhost:${port}${server.graphqlPath}`
  )
)
