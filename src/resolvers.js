const fetch = require("node-fetch")

const BASE_URL = "https://api.unsplash.com"
const PHOTOS_URL = `/photos?client_id=${process.env["CLIENT_ID"]}`

async function photos(root, {}, { headers }) {
  const response = await fetch(`${BASE_URL}${PHOTOS_URL}`, {
    headers: {
      accept: "application/json"
    }
  })

  console.log(response.headers.get("link"))
  return await response.json()
}

const Query = {
  hello: () => "Hello world!",
  photos: photos
}

module.exports = { Query }
